from aiohttp import web
from recipes_api.config import MEALDB_APIKEY
from recipes_api.integrations.themealdb import TheMealDB
from recipes_api.integrations.recipepuppy import RecipePuppy

from .helpers import valid_api_key, cached, cache_resp, save_log


recipes_routes = web.RouteTableDef()


# only access api-key in query
@recipes_routes.post('/recipes/search')
@valid_api_key
@cached
async def search(request):
    data = await request.json()
    query = data['query']
    if not query:
        return web.json_response({"status": "ERROR", "results": 'Invalid query.'}, status=400)

    themealdb_results = await TheMealDB(api_key=MEALDB_APIKEY).search(query=query)
    recipepuppy_results = await RecipePuppy().search(query=query)

    resp = {"status": "OK", "results": themealdb_results + recipepuppy_results}

    await cache_resp(query, resp, redis=request.app['redis'])
    await save_log(request.app['pg'], request.user, query, request.start_date)
    return web.json_response(resp)
