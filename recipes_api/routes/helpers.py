import json
from aiohttp import web
from functools import wraps
from datetime import datetime

from ..config import REDIS_PREFIX, REDIS_CACHE_TTL


def valid_api_key(func):
    @wraps(func)
    async def wrapper(request, *args, **kwargs):
        api_key = request.url.query.get('api_key')

        if not api_key:
            return web.json_response({"status": "ERROR", "results": 'No api-key provided.'}, status=400)

        user = await request.app['pg'].fetchrow(f"SELECT * FROM users WHERE api_key='{api_key}'")
        if not user:
            return web.json_response({"status": "ERROR", "results": 'Invalid api-key.'}, status=400)

        request.user = user
        request.start_date = datetime.now()
        return await func(request, *args, **kwargs)
    return wrapper


def cached(func):
    @wraps(func)
    async def wrapper(request, *args, **kwargs):
        redis = request.app['redis']
        data = await request.json()
        query = data.get('query')
        if not redis or not query:
            return await func(request, *args, **kwargs)

        cached_resp = await redis.get(f'{REDIS_PREFIX}:{query}', encoding="utf-8")
        if cached_resp:
            cached_resp = json.loads(cached_resp)
            print('From cache')
            await save_log(request.app['pg'], request.user, query, request.start_date)
            return web.json_response(cached_resp)

        print('Not in cache')
        return await func(request, *args, **kwargs)
    return wrapper


async def cache_resp(key, val, redis=None, to_json=True):
    if to_json:
        val = json.dumps(val)
    await redis.set(f'{REDIS_PREFIX}:{key}', val)
    await redis.expire(f'{REDIS_PREFIX}:{key}', REDIS_CACHE_TTL)


async def save_log(pg, user, query, start_date):
    sql_query = f"INSERT INTO logs (user_id, query, start_date, end_date) VALUES ({user.get('id')}, '{query}', '{start_date}', '{datetime.now()}')"
    print(sql_query)
    await pg.fetchrow(sql_query)
