import os

SERVER_PORT = os.getenv("SERVER_PORT", "8080")

DB_HOST = os.getenv("DB_HOST", "localhost")
DB_PORT = os.getenv("DB_PORT", "5432")
DB_NAME = os.getenv("DB_NAME", "")
DB_USER = os.getenv("DB_USER", "postgres")
DB_PASSWORD = os.getenv("DB_PASSWORD", "postgres")

REDIS_ADDRESS = os.getenv("REDIS_ADDRESS", "redis://localhost")
REDIS_DB = os.getenv("REDIS_DB", 7)
REDIS_PREFIX = os.getenv("REDIS_PREFIX", "recipes")
# in seconds
REDIS_CACHE_TTL = os.getenv("REDIS_PREFIX", 60)

MEALDB_APIKEY = os.getenv("MEALDB_APIKEY", "1")
