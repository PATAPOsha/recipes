from .interface import RecipesProvider
import aiohttp
import json

# API Docs: http://www.recipepuppy.com/about/api/


class RecipePuppy(RecipesProvider):
    async def search(self, query):
        async with aiohttp.ClientSession() as session:
            url = f'http://www.recipepuppy.com/api/?q={query}'
            async with session.get(url) as resp:
                response = await resp.json(content_type='text/javascript')

            if not response.get('results'):
                return []

            results = []
            for x in response['results'][:5]:
                res = {
                    "name": x['title'],
                    "instructions": '',
                    "ingredients": x['ingredients'].split(', '),
                    "image_url": x['thumbnail']
                }
                results.append(res)

            return results
