from sqlalchemy import (
    MetaData,
    Table,
    Column,
    String,
    BigInteger,
    ForeignKey,
    DateTime)

metadata = MetaData()

users = Table(
    "users",
    metadata,
    Column("id", BigInteger, primary_key=True),
    Column("username", String(128), unique=True, nullable=False),
    Column("password", String(128), nullable=False),
    Column("api_key", String(36), unique=True, nullable=False)
)


logs = Table(
    'logs',
    metadata,
    Column("id", BigInteger, primary_key=True),
    Column("user_id", BigInteger, ForeignKey('users.id')),
    Column("query", String(256), nullable=False),
    Column('start_date', DateTime(), nullable=False),
    Column('end_date', DateTime(), nullable=False)
)
