import requests

# # test search
r = requests.post('http://localhost:8080/recipes/search',
                  params={'api_key': 'e70626f7-9ab6-4b69-9063-d20d5ec3f122'},
                  json={'query': 'omelet'})
print(r.text)

# # test register
# r = requests.post('http://localhost:8080/users/register', json={'username': 'pata', 'password': 'test'})
# print(r.text)

# # test /users/me
# r = requests.post('http://localhost:8080/users/me', json={'username': 'pata', 'password': 'test'})
# print(r.text)
